#include "stdafx.h"
#include "NamedPipeRequestProcessor.h"



//////////////////////////////////////////////////////////////////////////
// CNamedPipeRequestProcessor

CNamedPipeRequestProcessor::CNamedPipeRequestProcessor(NAMED_PIPE_RECVPROC callback, LPVOID params)
	: m_callback(callback)
	, m_params(params)
{
}



void CNamedPipeRequestProcessor::processRequest(CNamedPipeRequest *pRequest)
{
	// Process request - call client callback
	QByteArray response;
	m_callback(pRequest->m_data, response, m_params);

	// Write the response to the pipe. 
	DWORD cchResponse = response.length() + 1;
	DWORD cbResponse = cchResponse;
	DWORD cbBytesWritten = 0;
	WriteFile(pRequest->m_hPipe,			// handle to pipe 
		response.constData(),		// buffer to write from 
		cbResponse,					// number of bytes to write 
		&cbBytesWritten,			// number of bytes written 
		NULL);						// not overlapped I/O 

	// Cleanup
	delete pRequest;
}
