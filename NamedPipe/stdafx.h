#pragma once

#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN            // Exclude rarely-used stuff from Windows headers
#endif

#include <Windows.h>


// Windows definitions mess up Qt's()
#undef min
#undef max


#ifdef QT_GUI_LIB
    #include <QGuiApplication>
#else
    #include <QCoreApplication>
#endif

#ifdef QT_QML_LIB
    #include <QQmlApplicationEngine>
#endif

#include <QThread>
#include <QDebug>
#include <QTimer>
#include <QDateTime>


/*
#include <shlwapi.h>
#pragma comment(lib, "shlwapi.lib")

#include <strsafe.h>
#pragma comment(lib, "strsafe.lib")
*/


#ifdef _UNICODE
#if defined _M_IX86
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='x86' publicKeyToken='6595b64144ccf1df' language='*'\"")
#elif defined _M_IA64
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='ia64' publicKeyToken='6595b64144ccf1df' language='*'\"")
#elif defined _M_X64
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='amd64' publicKeyToken='6595b64144ccf1df' language='*'\"")
#else
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")
#endif
#endif


