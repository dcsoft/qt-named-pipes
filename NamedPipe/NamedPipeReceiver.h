#pragma once

#include "NamedPipe.h"

class CNamedPipeReceiver : public QObject
{
	Q_OBJECT

public:
    CNamedPipeReceiver(CNamedPipe &pipe, NAMED_PIPE_RECVPROC recvProc, DWORD maxBufSize);

signals:
	void recvStopped();

public Q_SLOTS:
	void recv();

private:
	CNamedPipe &			m_hPipe;
	NAMED_PIPE_RECVPROC		m_recvProc;
    DWORD                   m_maxBufSize;
};
