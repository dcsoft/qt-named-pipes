#include "stdafx.h"
#include <AccCtrl.h>
#include <Aclapi.h>

#include "NamedPipe.h"



#define SEND_PIPE_TIMEOUT_MS		(NMPWAIT_WAIT_FOREVER)		// Match the .NET website behavior
#define RECV_BUFSIZE				(200000)	// TODO:  investigate working around max message size





//////////////////////////////////////////////////////////////////////////
// CNamedPipe

CNamedPipe::CNamedPipe(const QString &pipeName)
	: m_bReceiving(false)
	, m_bStopReceiving(false)
	, m_requestProcessor(nullptr)
{
	m_strPipeName = pipeName;
}



CNamedPipe::~CNamedPipe()
{
}




CNamedPipe::RESULT CNamedPipe::Send(const QByteArray &request, QByteArray &response)
{
	// Source:  Transactions on Named Pipes
	// http://msdn.microsoft.com/en-us/library/aa365789%28v=VS.85%29.aspx
	DWORD dwResponseLength;

	int cb = RECV_BUFSIZE;
	int cch = cb;
	response.resize(cch + 1);

	BOOL bOK = CallNamedPipe( (LPCWSTR) PipeName().utf16(), (LPVOID) request.constData(), request.length(),
							  response.data(), RECV_BUFSIZE, &dwResponseLength, SEND_PIPE_TIMEOUT_MS);

    //if ( !bOK )
    //{
    //	DWORD dw = GetLastError();
    //}

	return (bOK) ? SUCCESS : ERROR_SENDING;
}


/* static */ HANDLE CNamedPipe::CreateNamedPipeForEveryone(LPCWSTR pipeName)
{
	// Source:  https://groups.google.com/d/msg/microsoft.public.vc.mfc/xcIeL_NiacQ/cWjSIG1yeVkJ
	HANDLE hObject = INVALID_HANDLE_VALUE;
	PSID pEveryoneSID = NULL;
	PACL pACL = NULL;
	PSECURITY_DESCRIPTOR pSD = NULL;
	SID_IDENTIFIER_AUTHORITY SIDAuthWorld = SECURITY_WORLD_SID_AUTHORITY;
	SECURITY_ATTRIBUTES sa;

	// Create a well-known SID for the Everyone group.
	if (!AllocateAndInitializeSid(&SIDAuthWorld, 1,
		SECURITY_WORLD_RID,
		0, 0, 0, 0, 0, 0, 0,
		&pEveryoneSID))
	{
		return hObject;
	}

	EXPLICIT_ACCESS ea;
	ZeroMemory(&ea, sizeof(EXPLICIT_ACCESS));
	ea.grfAccessPermissions = STANDARD_RIGHTS_ALL | SPECIFIC_RIGHTS_ALL;
	ea.grfAccessMode = SET_ACCESS;
	ea.grfInheritance = NO_INHERITANCE;
	ea.Trustee.TrusteeForm = TRUSTEE_IS_SID;
	ea.Trustee.TrusteeType = TRUSTEE_IS_WELL_KNOWN_GROUP;
	ea.Trustee.ptstrName = (LPTSTR)pEveryoneSID;

	// Create a new ACL that contains the new ACE.
	DWORD dwRes = SetEntriesInAcl(1, &ea, NULL, &pACL);
	if (ERROR_SUCCESS != dwRes)
	{
		goto Cleanup;
	}

	// Initialize a security descriptor.

	pSD = (PSECURITY_DESCRIPTOR)LocalAlloc(LPTR,
		SECURITY_DESCRIPTOR_MIN_LENGTH);
	if (pSD == NULL)
	{
		goto Cleanup;
	}

	if (!InitializeSecurityDescriptor(pSD, SECURITY_DESCRIPTOR_REVISION))
	{
		goto Cleanup;
	}

	// Add the ACL to the security descriptor.

	if (!SetSecurityDescriptorDacl(pSD,
		TRUE,     // fDaclPresent flag
		pACL,
		FALSE))   // not a default DACL
	{
		goto Cleanup;
	}

	// Initialize a security attributes structure.

	sa.nLength = sizeof(SECURITY_ATTRIBUTES);
	sa.lpSecurityDescriptor = pSD;
	sa.bInheritHandle = FALSE;

	// Create your semaphore using 'sa'
	hObject = CreateNamedPipe(	pipeName,	// pipe name 
									PIPE_ACCESS_DUPLEX |	  // read/write access 	
									FILE_FLAG_OVERLAPPED,
									PIPE_TYPE_MESSAGE |       // message type pipe 
									PIPE_READMODE_MESSAGE |   // message-read mode 
									PIPE_WAIT,                // blocking mode 
									PIPE_UNLIMITED_INSTANCES, // max. instances  
									RECV_BUFSIZE,             // output buffer size 
									RECV_BUFSIZE,             // input buffer size 
									0,                        // client time-out - use default of 50 ms
									&sa);                     // Everyone security attribute 
Cleanup:
	if (pSD)
		LocalFree(pSD);

	if (pACL)
		LocalFree(pACL);

	if (pEveryoneSID)
		FreeSid(pEveryoneSID);

	return hObject;
}

CNamedPipe::RESULT CNamedPipe::Recv( NAMED_PIPE_RECVPROC callback, LPVOID params, DWORD maxBufSize )
{
	if ( m_bReceiving)
		return ERROR_RECEIVING_ALREADY;

	m_bReceiving = true;

	QThread *pRequestProcessorThread = new QThread();
	m_requestProcessor = new CNamedPipeRequestProcessor(callback, params);
	m_requestProcessor->moveToThread(pRequestProcessorThread);
	QObject::connect(this, &CNamedPipe::requestReceived, m_requestProcessor, &CNamedPipeRequestProcessor::processRequest);
	pRequestProcessorThread->start();


	// Source:  Multithreaded Pipe Server
	// http://msdn.microsoft.com/en-us/library/aa365588%28v=VS.85%29.aspx

	// The main loop creates an instance of the named pipe and 
	// then waits for a client to connect to it. When the client 
	// connects, a thread is created to handle communications 
	// with that client, and this loop is free to wait for the
	// next client connect request. It is an infinite loop.
	HANDLE hPipe = INVALID_HANDLE_VALUE;
	for (;;) 
	{ 
		hPipe = CreateNamedPipeForEveryone((LPCWSTR)PipeName().utf16());
		if (hPipe == INVALID_HANDLE_VALUE)
		{
			m_bReceiving = false;
			return ERROR_CREATENAMEDPIPE;
		}


		// Wait for the client to connect; if it succeeds, 
		// the function returns a nonzero value. If the function
		// returns zero, GetLastError returns ERROR_PIPE_CONNECTED. 
		BOOL connected = ConnectNamedPipe(hPipe, NULL) ? TRUE : (GetLastError() == ERROR_PIPE_CONNECTED);
		if ( !connected )
		{
			// The client could not connect, so close the pipe. 
			m_bReceiving = false;

			CloseHandle(hPipe);
			return ERROR_CONNECTNAMEDPIPE;
		}

		if ( m_bStopReceiving )		// StopRecv() has sent dummy data to wake us up; abort
		{
			// Shutdown receive thread
			pRequestProcessorThread->quit();
			pRequestProcessorThread->wait();
			delete pRequestProcessorThread;
			delete m_requestProcessor;
			m_requestProcessor = nullptr;

			// Flush the pipe to allow the client to read the pipe's contents 
			// before disconnecting. Then disconnect the pipe, and close the 
			// handle to this pipe instance. 
			FlushFileBuffers(hPipe);
			DisconnectNamedPipe(hPipe);
			CloseHandle(hPipe);

			m_bReceiving = false;
			return SUCCESS;
		}

		// Quickly read request and process it in the worker thread.
		// The Qt signal/slot system buffers all requests and delivers them to the slot(s)
		// connected to the requestReceived signal.
        // Delivery occurs when the QThread cycles through the event loop.
		// This allows this for(;;) loop to continue executing, potentially creating more
		// requests, which are executed by the Request Thread, in the order received.
        CNamedPipeRequest *pRequest = ReadRequest(hPipe, maxBufSize);
		emit requestReceived(pRequest);
	} 

	m_bReceiving = false;
	return SUCCESS; 
} 


CNamedPipeRequest *CNamedPipe::ReadRequest(HANDLE hPipe, DWORD maxBufSize)
{
	// Read client requests from the pipe. This simplistic code only allows messages
    // up to maxBufSize characters in length.
    CNamedPipeRequest *pRequest = new CNamedPipeRequest(hPipe, maxBufSize);
	DWORD cbBytesRead = 0;
	BOOL success = ReadFile(hPipe,						// handle to pipe 
							pRequest->m_data.data(),	// buffer to receive data 
                            maxBufSize,				// size of buffer
							&cbBytesRead,				// number of bytes read 
							NULL);						// not overlapped I/O 

    if (success)
        pRequest->m_data.resize((int)cbBytesRead);
	return pRequest;
}



CNamedPipe::RESULT CNamedPipe::StopRecv()
{
	if ( !m_bReceiving )		// not even receiving
		return SUCCESS;

	if ( m_bStopReceiving )	// trying to stop receiving already
		return ERROR_STOP_RECEIVING_ALREADY;

	m_bStopReceiving = true;

	// Send dummy data into pipe so that Recv() quits
	CNamedPipe tempPipe(m_strPipeName);

	// The Send() may fail as Recv() may not correctly respond
	// to the request, since m_bStopReceiving is true.
	// That's OK, we just wanted to wake up Recv() so it can return.
	// Ignore return code.
	QByteArray response;
	Send("Please stop", response);


	// wait for Recv() to quit
	while ( m_bReceiving )
		QThread::currentThread()->msleep(200);

	m_bStopReceiving = false;

	return SUCCESS;
}



QString CNamedPipe::PipeName()
{
	return QString("\\\\.\\pipe\\%1").arg(m_strPipeName);
}
