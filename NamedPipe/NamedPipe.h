#pragma once

#include "NamedPipeRequestProcessor.h"


class RequestInfo;



//////////////////////////////////////////////////////////////////////////
// CNamedPipe

class CNamedPipe : public QObject
{
	Q_OBJECT

public:
	enum RESULT
	{
		SUCCESS,

		// Errors during Send()
		ERROR_SENDING,

		// Errors during Recv()
		ERROR_RECVTHREAD_MEMORY,
		ERROR_CREATENAMEDPIPE,
		ERROR_CONNECTNAMEDPIPE,
		ERROR_BEGINTHREAD,

		ERROR_RECVTHREAD_CLIENTDISCONNECTED,

		ERROR_RECEIVING_ALREADY,
		ERROR_STOP_RECEIVING_ALREADY,
	};

    CNamedPipe(const QString &pipeName = QString("com.dcsoft.demo.namedpipe"));
	~CNamedPipe();

	RESULT Send(const QByteArray &request, QByteArray &response);
    RESULT Recv(NAMED_PIPE_RECVPROC callback, LPVOID params, DWORD maxBufSize);
	RESULT StopRecv();

signals:
	void requestReceived(CNamedPipeRequest *pRequest);

protected:

	QString PipeName();
	void ClosePipe();

	static HANDLE CreateNamedPipeForEveryone(LPCWSTR pipeName);

    CNamedPipeRequest *ReadRequest(HANDLE hPipe, DWORD maxBufSize);

	// Communication between thread calling StopRecv(), thread running Recv(), and threads spawned from Recv()
	// to cleanly shutdown when StopRecv() is called
	volatile bool m_bReceiving;		// whether currently Recv() is running
	volatile bool m_bStopReceiving;	// whether currently StopRecv() is running

	QString						m_strPipeName;

	CNamedPipeRequestProcessor	*m_requestProcessor;

	Q_DISABLE_COPY(CNamedPipe)		// because contained QMutex and QSemaphore can't be copied
};
