#include "stdafx.h"
#include "NamedPipeReceiver.h"



CNamedPipeReceiver::CNamedPipeReceiver(CNamedPipe &pipe, NAMED_PIPE_RECVPROC recvProc, DWORD maxBufSize)
	: m_hPipe(pipe)
	, m_recvProc(recvProc)
    , m_maxBufSize(maxBufSize)
{
}


void CNamedPipeReceiver::recv()
{
	// Call named pipe recv() to receive packets on this thread
    CNamedPipe::RESULT result = m_hPipe.Recv(m_recvProc, nullptr, m_maxBufSize);

	// Program is ending, Recv() has finished
	if (result != CNamedPipe::SUCCESS)
		qDebug() << tr("Failed named pipe Recv: %1").arg(result);

	emit recvStopped();
}
