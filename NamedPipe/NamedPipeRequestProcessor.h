#pragma once


// Requests and Responses are in UTF-16 (Unicode)
typedef void(*NAMED_PIPE_RECVPROC)(const QByteArray &request, QByteArray &response, LPVOID params);


//////////////////////////////////////////////////////////////////////////
// CNamedPipeRequest

class CNamedPipeRequest
{
public:
    CNamedPipeRequest(HANDLE hPipe, DWORD maxRequestSize)
		: m_hPipe(hPipe)
        , m_data((int)maxRequestSize, Qt::Initialization::Uninitialized)
	{
	}

	HANDLE		m_hPipe;
	QByteArray  m_data;
};




//////////////////////////////////////////////////////////////////////////
// CNamedPipeRequestProcessor

class CNamedPipeRequestProcessor : public QObject
{
	Q_OBJECT

public:
	CNamedPipeRequestProcessor(NAMED_PIPE_RECVPROC callback, LPVOID params);

public Q_SLOTS:
	void processRequest(CNamedPipeRequest *pInfo);

protected:
	NAMED_PIPE_RECVPROC			m_callback;
	LPVOID						m_params;
};




