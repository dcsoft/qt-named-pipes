# Qt Named Pipe for Windows

## Goals

1.  To provide a Qt/C++ implementation of Windows Named Pipes.
2.  To demonstrate Named Pipe communication with a simple Server and Client.
3.  To practice QML techniques in the Client.
4.  To practice multi-threaded techniques in the Server and in the Named Pipe implementation.

## Introduction
One of the most robust mechanisms for Windows IPC (interprocess communication) is Named Pipes.  Named Pipes are similar to TCP/IP sockets, which provide a channel for a client (e.g. a web browser) to send a request (e.g. GET 'http://dcsoft.com') to the Server, which does work (format the html page), and sends back a response (text/html representation of the DC Software home page).

But Named Pipes are superior to TCP/IP sockets because:

1.  They don't require picking a random port which could be in use.  Instead, you can pick any name, including a GUID.
2.  They don't require interacting with the Windows Firewall to let the chosen port be opened.
3.  They are very fast.  In fact, like sockets, they are implemented over TCP.

Unfortunately, Qt does not provide a QWindowsNamedPipe.  This project fills that void.

## Stability

The guts of the named pipe have shipped in production, but the Qt wrapper (and therefore the overall project) is Beta quality.




## Usage
0.  Build client.pro and server.pro.
1.  Start Server (server.exe is compiled to the "NamedPipe-Deliver\ folder)
2.  Start Client (client.exe is compiled to the "NamedPipe-Deliver\ folder)

### 1.  Server
The Server is like the web server in the above example.  It receives (text) requests from the client and sends back (text) responses.  Our Server simply echoes the request back in the response, tacking on an incrementing count.

![Named Pipe Server](Images/named-pipe-server-ss.gif "Named Pipe Server")

The server is a command-line program that listens on the named pipe and prints the responses it sends back.  Here, the client request is "Hello from the Client!".  The response is "Hello from the Client!:  0" (the 0 is the incrementing count which is currently set to the initial 0, as it is the first request.)


### 2.  Client
The Client is like the web browser in the above example.  It sends (text) requests to the server and prints the (text) response.

![Named Pipe Client](Images/named-pipe-client-ss.gif "Named Pipe Client")


Type "Hello from the Client!" into the TextField at the bottom.  Press RETURN.

The text is sent to the server over the named pipe.  The server sends a response containing the same text with an incrementing count appended.  The response is shown in the top (gray) TextArea.

Type additional text and press RETURN.  The responses continue to accumulate:

![Named Pipe Client 2](Images/named-pipe-client-ss-2.gif "Named Pipe Client 2")

OPTIONAL:  Like a web server, our server can handle simultaneous requests from multiple clients.  So fire up some more Client.exe instances and see how they share the incrementing counter as each request is sent from whichever client.

## Implementation

### Threading

The Server uses 2 worker `QThread`s.  First, the server runs the `NamedPipeReceiver` on its own thread so that it can sit in a loop waiting for the next request to arrive, without hanging the process.  Second, the server runs the `NamedPipeRequestProcessor` on its own thread to process the incoming requests, in order of receipt. 

These two threading implementations demonstrate [the recommended technique to avoid subclassing QThread](http://blog.qt.io/blog/2010/06/17/youre-doing-it-wrong/).

Unfortunately, because the server is a command-line (and not GUI) program, it doesn't really show off this multi-threading.
  

### Buffer Limits

The Named Pipe implementation uses a hard-coded `RECV_BUFSIZE` set to 200,000 bytes, which is passed to both `CreateNamedPipe` and `CallNamedPipe`.  Although `QByteArray` can grow as needed, I don't know off-hand if we can easily make this limit dynamic.

Another buffer size limit is a hard-coded `MAX_PIPE_BUFSIZE` set to 8 KB.  It is passed to `CNamedPipeReceiver::maxBufSize`.

TODO:  more fully understand these limits and perhaps eliminate at least one of them.

## Exposing data to QML
The Client has `myData.h` which is the only C++ class exposed to QML.  It provides data to be displayed by QML and sinks input data from QML.  It uses `Q_DECLARE_METATYPE` to interop with QML and exposes a `Q_PROPERTY`, a `signal` that updates the QML when the property has changed, and a `slot` that the QML calls to send the text to the pipe.
