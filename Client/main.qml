import QtQuick 2.11
import QtQuick.Controls 2.4
//import QtQuick.Layouts 1.3
import MyData 1.0       // https://stackoverflow.com/a/41901671/2073217

ApplicationWindow {
    id: window
    visible: true
    width: 640
    height: 480
    title: qsTr("Named Pipes")

    Column {
        anchors.margins: 5

        Rectangle {
            id: rectangle
            height: window.height - rectangle2.height
            width: window.width
            border.color: "green"
            color: "gray"

            ScrollView {
                anchors.fill: parent
                ScrollBar.vertical.policy: ScrollBar.AlwaysOn

                TextArea {
                    readOnly: true
                    textFormat: "PlainText"
                    text: MyData.Messages       // https://stackoverflow.com/a/41901671/2073217
                }
            }
        }

        Rectangle {
            id: rectangle2
            height: textField.height + 10
            width: window.width
            color: "steelblue"

            TextField {
                id:  textField
                anchors.centerIn: rectangle2
                width: rectangle2.width - 10
                placeholderText: "Type here..."

                onAccepted: MyData.onSendMessage(textField.text)
            }
        }
    }
}
