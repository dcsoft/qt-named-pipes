#ifndef MYDATA_H
#define MYDATA_H

#include "NamedPipe/stdafx.h"
#include "NamedPipe/NamedPipeReceiver.h"


class MyData : public QObject
{
    /*
     * The only C++ class exposed to our QML.  Provides data to be displayed by QML and sinks input data from QML
     */

    Q_OBJECT


    // A string of newline-separated messages received from the server, over the pipe
    Q_PROPERTY(QString Messages MEMBER m_messages NOTIFY messagesChanged)

public:
    // Default ctor, Copy ctor, and Dtor required for Q_DECLARE_METATYPE
    MyData() {}
    MyData(const MyData &other);
    ~MyData() {}

    // append a new message from the server, to the string of existing newline-separated messages
    void appendMessage(QString newMessage)
    {
        m_messages += newMessage + "\n";
        emit messagesChanged();
    }

signals:
    void messagesChanged();     // so QML is updated when message is appended

public slots:
    void onSendMessage(QString newMessage)
    {
        // Called by QML to send a message out pipe
        qDebug() << QString("Sending: %1").arg(newMessage);

        QByteArray response;
        m_namedPipe.Send(newMessage.toLatin1(), response);

        QString responseStr(response);
        appendMessage(responseStr);

        qDebug() << QString("Response: %1").arg(responseStr);
        qDebug() << "---";
    }

private:
    QString m_messages;
    CNamedPipe m_namedPipe;
};


Q_DECLARE_METATYPE(MyData);

#endif // MYDATA_H
