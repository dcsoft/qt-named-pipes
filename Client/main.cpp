#include "NamedPipe/stdafx.h"
#include "mydata.h"




// https://stackoverflow.com/a/41901671/2073217
static MyData myData;

static QObject *getMyData(QQmlEngine *e, QJSEngine *) {
  e->setObjectOwnership(&myData, QQmlEngine::CppOwnership); // just in case
  return &myData;
}

int main(int argc, char *argv[])
{
    // Workaround Qt 5.11 bug with Intel UHD graphics chips - https://bugreports.qt.io/browse/QTBUG-64697
    QCoreApplication::setAttribute(Qt::AA_UseSoftwareOpenGL);

    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);


    QGuiApplication app(argc, argv);


    // Instantiate the class exposed to QML
    // https://stackoverflow.com/a/41901671/2073217
    qmlRegisterSingletonType<MyData>("MyData", 1, 0, "MyData", getMyData);

    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty())
    {
        printf("No root objects");
        return -1;
    }

    return app.exec();
}
