#include "NamedPipe/stdafx.h"
#include "NamedPipe/NamedPipeReceiver.h"
#include <QTimer>


#define MAX_PIPE_BUFSIZE    (8 * 1024)      // the max message length that can be received on the named pipe.  See if this limit can be removed!!!!


static void OnServerNamedPipeRecv(const QByteArray &request, QByteArray &response, LPVOID /*params*/)
{
    // A request has been received from the client
    // Send a response with an 'echo' of the text of the request, with an incrementing count appended to it
    static int count = 0;

    QString strRequest(request);

    // Create the response - echo the text of the request and append the count
    QString strResponse = QString("%1: %2").arg(strRequest).arg(count++);
    response = strResponse.toLatin1();

    // Log the output
    qDebug() << strResponse;
}


int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    // The named pipe instance that survives forever, until the program is quit (when a.exec() returns)
    CNamedPipe namedPipe;

    // An easy way to execute code after the QCoreApplication has started processing events is to get called back
    // on a single shot timer
    QTimer::singleShot(0, [&namedPipe]() {
        // Start listening on named pipe
        CNamedPipeReceiver *namedPipeReceiver = new CNamedPipeReceiver(namedPipe, OnServerNamedPipeRecv, MAX_PIPE_BUFSIZE);
        QThread *receiveThread = new QThread();
        namedPipeReceiver->moveToThread(receiveThread);
        QObject::connect(receiveThread, &QThread::started, namedPipeReceiver, &CNamedPipeReceiver::recv);
        QObject::connect(namedPipeReceiver, &CNamedPipeReceiver::recvStopped, receiveThread, &QThread::quit);
        QObject::connect(receiveThread, &QThread::finished, receiveThread, &QObject::deleteLater);
        QObject::connect(receiveThread, &QThread::finished, namedPipeReceiver, &QObject::deleteLater);
        receiveThread->start();

        // Wait until recv thread starts
        while (!receiveThread->isRunning())
            QThread::currentThread()->msleep(100);

        qDebug() << "Listening on named pipe...";
    });

    return a.exec();
}
